Rails.application.routes.draw do
  resources :phones, only: [:index, :show] do
    get 'select', on: :collection
  end
  get 'welcome/index'

  root 'welcome#index'
end
