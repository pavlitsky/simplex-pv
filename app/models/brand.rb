# Brand model. Acts as Active Model.
class Brand
  include ActiveModel::Model

  # Brand code
  attr_accessor :code
  # Brand name
  attr_accessor :name

  # Returns array of all brands available
  def self.all
    Provider.new.brands.map { |brand| Brand.new brand }
  end
end
