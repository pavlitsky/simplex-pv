# Phone model. Acts as Active Model.
class Phone
  include ActiveModel::Model

  # Phone's uniq code used as a key.
  attr_accessor :code
  # Phone's display name.
  attr_accessor :name
  # Phone's brand object.
  attr_accessor :brand
  # Phone's brand code.
  attr_accessor :brand_code
  # Phone's brand name.
  attr_accessor :brand_name
  # Phone's image URL.
  attr_accessor :image_url
  # Phone's specs array.
  attr_accessor :specs

  # Returns phones array by brand
  def self.by_brand(brand)
    Provider.new.phones(brand).map { |phone| Phone.new phone }
  end

  # Returns phones array by query phrase
  def self.search(query)
    Provider.new.search(query).map { |phone| Phone.new phone }
  end

  # Loads phone +specs+ attribute from provider
  def load_specs
    self.specs = Provider.new.phone(self).specs
  end
end
