$(document).on 'change', '#brands-select', ->
  brand_code = encodeURIComponent($(this).val())
  populate_phones (brand_code) if brand_code
  clear_phones_select()

$(document).on 'change', '#phones-select', ->
  code = $(this).val()
  show_phone(code) if code
  clear_phone_container()

$(document).on 'input', '#phone-search-input', ->
  code = $(this).val()
  query_phones(code) if code
  clear_phones_container()

window.clear_phone_container = ->
  $('#phone-container').empty()

window.clear_phones_container = ->
  $('#phones-container').empty()

window.clear_phones_select = ->
  $('#phones-select-container').empty()
  clear_phone_container()
