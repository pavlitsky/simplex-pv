window.query_phones = (code) ->
  $.ajax({url: '/phones?query=' + code, dataType: 'script'})

window.show_phone = (code) ->
  $.ajax({url: '/phones/' + code, dataType: 'script'})

window.populate_phones = (brand_code) ->
  $.ajax({url: '/phones/select?brand_code=' + brand_code, dataType: 'script'})
