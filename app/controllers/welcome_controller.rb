# Main controller
class WelcomeController < ApplicationController
  # Index action, gets all brands
  def index
    @brands = Brand.all
  end
end
