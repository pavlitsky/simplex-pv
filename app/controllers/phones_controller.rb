# Controller for Phone
class PhonesController < ApplicationController
  before_action :set_brand, only: [:select]
  before_action :set_phone, only: [:show]

  # Gets phones found by params[:query] or empty array if query is empty
  def index
    @query = (params[:query] || '').strip
    @phones = @query.present? ? Phone.search(@query) : []
  end

  # Loads phone specifications and renders it
  def show
    @phone.load_specs
  end

  # Returns data needed for select_tag
  def select
    @phones = Phone.by_brand @brand
  end

  private

  def set_brand
    @brand = Brand.new code: params[:brand_code]
  end

  def set_phone
    @phone = Phone.new code: params[:id]
  end
end
