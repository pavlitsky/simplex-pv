# Phone catalog providers fabric
class Provider
  # Default provider used when object initialized without parameters
  DEFAULT_PROVIDER = 'gsm_arena'.freeze

  # Provider instance used in fabric methods
  attr_accessor :provider

  # Initializes provider instance from parameter
  def initialize(provider = DEFAULT_PROVIDER)
    case provider
    when 'gsm_arena' then @provider = Providers::GsmArenaProvider.new
    else raise NameError, "Unknown provider: #{provider}"
    end
  end

  # Calls +brands+ method of provider and returns it's result
  def brands
    @provider.brands
  end

  # Calls +phones+ method of provider and returns it's result
  def phones(brand)
    @provider.phones brand
  end

  # Calls +phone+ method of provider and returns it's result
  def phone(phone)
    @provider.phone phone
  end

  # Calls +search+ method of provider and returns it's result
  def search(query)
    @provider.search query
  end
end
