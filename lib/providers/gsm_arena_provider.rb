require 'open-uri'

class Providers
  # "GSM Arena" phones catalog provider
  class GsmArenaProvider
    # "GSM Arena" hostname
    HOST_URL = 'http://www.gsmarena.com'.freeze

    # Get all available phone brands
    # Returns array of hashes with +code+ and +name+ of brand.
    def brands
      html = open "#{HOST_URL}/makers.php3"
      HTMLScraper.new(html).scrape_each '//table/tr/td/a' do |node|
        code = code_from_node node
        name = node.at_xpath('.//text()[1]').content
        { code: code, name: name } if name && code
      end
    end

    # Get phones array by +brand+
    def phones(brand)
      url = "#{brand.code}.php"
      result = []
      # Cycle through pagination
      loop do
        scraper = HTMLScraper.new open("#{HOST_URL}/#{url}")
        result += phones_list(scraper)
        url = scraper.scrape('//a[@class="pages-next" and not(@class="disabled")]') { |node| node['href'] }
        break unless url
      end
      result
    end

    # Fill details of passed +phone+
    def phone(phone)
      scraper = HTMLScraper.new open("#{HOST_URL}/#{phone.code}.php")
      phone.specs = phone_specs scraper
      phone.image_url = phone_image scraper
      phone
    end

    # Filter phones by name and return array of hashes
    def search(query)
      html = open("#{HOST_URL}/results.php3?sName=#{CGI.escape query}")
      HTMLScraper.new(html).scrape_each('//*[@class="section-body"]/*/ul/li/a') do |node|
        code = code_from_node node
        brand = brand_from_search_node node
        name = name_from_search_node node
        image = node.at_xpath('.//img')
        { code: code, name: name, brand_name: brand, image_url: image['src'] } if code && image && name && brand
      end || []
    end

    private

    # Get phones list from html page
    def phones_list(scraper)
      scraper.scrape_each '//*[@class="section-body"]//ul/li/a' do |node|
        code = code_from_node node
        name = node.at_xpath('.//span').content
        { code: code, name: name } if name && code
      end
    end

    # Get code from node href
    def code_from_node(node)
      code_match = /(?<code>.*)\.php/.match node['href']
      return nil unless code_match
      code_match[:code]
    end

    # Get brand name from search node
    def brand_from_search_node(node)
      brand = node.at_xpath('.//span')
      return unless brand.present?
      brand.children.to_s.split('<br>').try(:first)
    end

    # Get phone name from search node
    def name_from_search_node(node)
      name = node.at_xpath('.//span')
      return unless name.present?
      name.children.to_s.split('<br>').try(:last)
    end

    # Get phone specifications from scraper
    def phone_specs(scraper)
      scraper.scrape_each '//*[@id="specs-list"]/table' do |node|
        group = node.xpath('.//th').first.content || ''
        group_specs = node.xpath('.//tr').each_with_object([]) do |spec_node, all_specs|
          spec_title = spec_node.at_xpath('.//td[@class="ttl"]').try(:content)
          spec_info = spec_node.at_xpath('.//td[@class="nfo"]').try(:content)
          all_specs << { title: spec_title, info: spec_info }
        end
        { group: group, specs: group_specs }
      end
    end

    # Get phone image from scraper
    def phone_image(scraper)
      scraper.scrape '//*[@class="specs-photo-main"]//img' do |node|
        node['src']
      end
    end
  end
end
