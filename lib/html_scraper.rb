##
# Utility for HTML traversing via XPATH expressions.
# === Examples
#   HTMLScraper.new(html).scrape_each(xpath) { |node| { link: node['href'] } } # => Returns array of hashes with link keys set
class HTMLScraper
  # Nokogiri document initialized with HTML text
  attr_accessor :doc

  # Initialize object. +html+ parameter type can be +String+ or +IO+.
  def initialize(html)
    @doc = Nokogiri::HTML(html)
  end

  # Finds +xpath+ element in HTML and yields it to +block+.
  # Returns yield result.
  def scrape(xpath, &block)
    return unless block
    node = @doc.at_xpath xpath
    return if node.blank?
    yield node
  end

  # Finds elements in HTML by +xpath+ and yields it to the +block+ in cycle.
  # Returns array of yields results.
  def scrape_each(xpath, &block)
    return unless block
    nodes = @doc.xpath xpath
    return if nodes.blank?
    nodes.each_with_object([]) do |e, a|
      result = yield e
      result ? a << result : a
    end
  end
end
