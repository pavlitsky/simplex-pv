require 'rails_helper'

RSpec.describe Brand, type: :model do
  describe 'provider method' do
    it '.all' do
      brands = [FactoryGirl.build(:brand_1), FactoryGirl.build(:brand_2)]
      expect(Brand.all.map { |b| [b.code, b.name] }).to match_array brands.map { |b| [b.code, b.name] }
    end
  end
end
