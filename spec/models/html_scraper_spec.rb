require 'rails_helper'

RSpec.describe HTMLScraper, type: :model do
  describe 'scrapes' do
    it 'succeeds on valid element html' do
      html = '<td><a href=brand1-phones-1.php>Brand 1 phones (10)</a></td>'
      xpath = '//td/a'
      brand = HTMLScraper.new(html).scrape(xpath) do |node|
        node['href']
      end
      expect(brand).to eql('brand1-phones-1.php')
    end
    it 'succeeds on valid array html' do
      html = '<td><a href=brand1-phones-1>Brand 1 phones (10)</a></td>'\
             '<td><a href=brand2-phones-2>Brand 2 phones (20)</a></td>'
      xpath = '//td/a'
      brand = HTMLScraper.new(html).scrape_each(xpath) do |node|
        { code: node['href'] }
      end
      expect(brand).to match_array [{ code: 'brand1-phones-1' }, { code: 'brand2-phones-2' }]
    end
    it 'fails on invalid html' do
      html = '<td><a href=brand1-phones-1.php>Brand 1 phones (10)</a></td>'\
             '<td><a href=brand2-phones-2.php>Brand 2 phones (20)</a></td>'
      xpath = '//td/div'
      expect(HTMLScraper.new(html).scrape_each(xpath) { |_node| {} }).to be nil
    end
  end
end
