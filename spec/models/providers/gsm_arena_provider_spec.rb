require 'rails_helper'

RSpec.describe Providers::GsmArenaProvider, type: :model do
  describe 'scrapes' do
    it 'brands' do
      brands = [FactoryGirl.build(:brand_1), FactoryGirl.build(:brand_2)]
      expect(Providers::GsmArenaProvider.new.brands).to match_array brands.map { |b| { code: b.code, name: b.name } }
    end
    it 'phones' do
      brand = FactoryGirl.build(:brand_1)
      phones = [FactoryGirl.build(:phone_1), FactoryGirl.build(:phone_2), FactoryGirl.build(:phone_3), FactoryGirl.build(:phone_4)]
      expect(Providers::GsmArenaProvider.new.phones(brand)).to match_array phones.map { |b| { code: b.code, name: b.name } }
    end
    it 'phone' do
      phone_1 = Providers::GsmArenaProvider.new.phone FactoryGirl.build :phone_1
      phone_2 = FactoryGirl.build :phone_1_with_specs
      expect(phone_1).to have_attributes(code: phone_2.code, name: phone_2.name, image_url: phone_2.image_url, specs: phone_2.specs)
    end
    it 'search' do
      phones = [FactoryGirl.build(:phone_1), FactoryGirl.build(:phone_2)]
      provider_phones = Providers::GsmArenaProvider.new.search('query')
      expect(provider_phones.map { |p| { code: p[:code], name: p[:name], image_url: p[:image_url] } })
        .to match_array phones.map { |p| { code: p.code, name: p.name, image_url: p.image_url } }
    end
  end
end
