require 'rails_helper'

RSpec.describe Provider, type: :model do
  describe 'init' do
    it 'returns provider' do
      provider = Provider.new
      expect(provider.provider).to be_instance_of Providers::GsmArenaProvider
    end
    it 'fails on unknown provider' do
      expect { Provider.new('unknown') }.to raise_error NameError
    end
  end
  describe 'respond to' do
    it 'interface methods' do
      expect(Provider.new).to respond_to :brands, :phones, :phone, :search
    end
  end
end
