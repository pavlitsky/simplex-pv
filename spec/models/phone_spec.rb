require 'rails_helper'

RSpec.describe Phone, type: :model do
  describe 'provider method' do
    it '.by_brand' do
      brand = FactoryGirl.build(:brand_1)
      phones = [FactoryGirl.build(:phone_1), FactoryGirl.build(:phone_2), FactoryGirl.build(:phone_3), FactoryGirl.build(:phone_4)]
      expect(Phone.by_brand(brand).map { |p| [p.code, p.name] }).to match_array phones.map { |p| [p.code, p.name] }
    end
    it '.load_specs' do
      phone_1 = FactoryGirl.build(:phone_1)
      phone_1_with_specs = FactoryGirl.build(:phone_1_with_specs)
      phone_1.load_specs
      expect(phone_1.specs).to eql phone_1_with_specs.specs
    end
    it '.search' do
      phones = [FactoryGirl.build(:phone_1), FactoryGirl.build(:phone_2)]
      expect(Phone.search('query').map { |p| [p.code, p.name] }).to match_array phones.map { |p| [p.code, p.name] }
    end
  end
end
