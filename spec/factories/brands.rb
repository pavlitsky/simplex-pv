FactoryGirl.define do
  factory :brand do
    factory :brand_1 do
      code 'brand1-phones-1'
      name 'Brand 1'
    end
    factory :brand_2 do
      code 'brand2-phones-2'
      name 'Brand 2'
    end
  end
end
