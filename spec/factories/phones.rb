FactoryGirl.define do
  factory :phone do
    factory :phone_1 do
      code 'brand-phone-1'
      name 'Phone 1'
      image_url 'phone-1.jpg'
      factory :phone_1_with_specs do
        specs [
          { group: 'Network', specs: [
            { title: 'Technology', info: 'GSM' },
            { title: '2G bands', info: 'GSM 850' },
            { title: '', info: 'CDMA 800' }
          ] },
          { group: 'Launch', specs: [
            { title: 'Announced', info: '2016' },
            { title: 'Status', info: 'Available' }
          ] }
        ]
      end
    end
    factory :phone_2 do
      code 'brand-phone-2'
      name 'Phone 2'
      image_url 'phone-2.jpg'
    end
    factory :phone_3 do
      code 'brand-phone-3'
      name 'Phone 3'
      image_url 'phone-3.jpg'
    end
    factory :phone_4 do
      code 'brand-phone-4'
      name 'Phone 4'
      image_url 'phone-4.jpg'
    end
  end
end
