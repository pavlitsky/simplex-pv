# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'webmock/rspec'
require 'support/factory_girl'
require 'simplecov'
SimpleCov.start

# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

# ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  # config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  # config.use_transactional_fixtures = true

  # Automatically mix in different behaviours to your tests based on their file location
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!

  config.raise_errors_for_deprecations!

  config.before(:each) do
    stub_request(:get, 'http://www.gsmarena.com/makers.php3')
      .to_return(status: 200, body: File.read("#{::Rails.root}/spec/support/fixtures/brands.html"), headers: {})
    stub_request(:get, 'http://www.gsmarena.com/brand1-phones-1.php')
      .to_return(status: 200, body: File.read("#{::Rails.root}/spec/support/fixtures/phones.html"), headers: {})
    stub_request(:get, 'http://www.gsmarena.com/brand-phone-1.php')
      .to_return(status: 200, body: File.read("#{::Rails.root}/spec/support/fixtures/phone.html"), headers: {})
    stub_request(:get, 'http://www.gsmarena.com/results.php3?sName=query')
      .to_return(status: 200, body: File.read("#{::Rails.root}/spec/support/fixtures/search.html"), headers: {})
    stub_request(:get, 'http://www.gsmarena.com/brand1-phones-1-p2.php')
      .to_return(status: 200, body: File.read("#{::Rails.root}/spec/support/fixtures/phones-p2.html"), headers: {})
  end
end
