require 'rails_helper'

RSpec.describe PhonesController, type: :controller do
  describe 'GET index' do
    it 'returns http success on query' do
      xhr :get, :index, query: 'query'
      expect(response).to have_http_status(:success)
    end
    it 'returns nothing on empty query' do
      xhr :get, :index, query: nil
      expect(assigns(:phones)).to eql []
    end
  end
  describe 'GET show' do
    it 'returns phone' do
      phone = FactoryGirl.build :phone_1_with_specs
      xhr :get, :show, id: phone.code
      expect(assigns(:phone).code).to eql phone.code
    end
  end
  describe 'GET select' do
    it 'returns phones' do
      xhr :get, :select, brand_code: 'brand1-phones-1'
      expect(response).to have_http_status(:success)
    end
  end
end
