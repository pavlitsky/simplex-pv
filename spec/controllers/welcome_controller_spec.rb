require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  describe 'GET index' do
    it 'assigns brands' do
      get :index
      brands = [FactoryGirl.build(:brand_1), FactoryGirl.build(:brand_2)]
      expect(assigns(:brands).map { |b| [b.code, b.name] }).to match_array brands.map { |b| [b.code, b.name] }
    end
    it 'renders welcome page' do
      get :index
      expect(response).to render_template(:index)
    end
  end
end
